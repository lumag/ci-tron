---
- name: Install minio-client
  ansible.builtin.shell: "wget --progres=dot:mega -O /usr/local/bin/mcli {{ minio_client_bin_url }} && chmod +x /usr/local/bin/mcli"
  args:
    creates: /usr/local/bin/mcli
  tags:
    - skip_ansible_lint
    - install

- name: Install a socket-activated version of minio
  ansible.builtin.shell: "wget --progres=dot:mega -O /usr/local/bin/minio {{ minio_bin_url }} && chmod +x /usr/local/bin/minio"
  args:
    creates: /usr/local/bin/minio
  tags:
    - skip_ansible_lint
    - install

- name: Adding minio firewall config
  become: true
  ansible.builtin.template:
    src: 'minio.nft.j2'
    dest: /etc/nftables.d/50-minio.nft
    owner: root
    group: root
    mode: '0644'
  notify: 'Restart nftables'

- name: Installing minio_configure script
  become: true
  ansible.builtin.template:
    src: 'minio_configure.sh.j2'
    dest: '/usr/local/bin/minio_configure'
    mode: '0755'
    owner: root
  notify: 'Restart minio configure'

- name: Installing minio_genpw script
  become: true
  ansible.builtin.template:
    src: 'minio_genpw.sh.j2'
    dest: '/usr/local/bin/minio_genpw'
    mode: '0755'
    owner: root

- name: Create the minio group
  become: true
  ansible.builtin.group:
    name: minio

- name: Create the minio user
  become: true
  ansible.builtin.user:
    name: minio
    group: minio
    shell: /bin/nologin

- name: Ensure minio configuration directory exists
  become: true
  ansible.builtin.file:
    mode: '0755'
    owner: minio
    group: minio
    path: /etc/minio
    state: directory

- name: Installing minio bucket policies
  become: true
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "minio"
    mode: '0644'
  loop:
    - { src: 'valvetraces.json.j2', dest: '/etc/minio/valvetraces.json' }
    - { src: 'valvetraces-ro.json.j2', dest: '/etc/minio/valvetraces-ro.json' }
    - { src: 'valvefossils.json.j2', dest: '/etc/minio/valvefossils.json' }
    - { src: 'valvefossils-ro.json.j2', dest: '/etc/minio/valvefossils-ro.json' }

- name: Ensure /etc/systemd/system/minio.service.d/ is gone (legacy)
  ansible.builtin.file:
    state: absent
    path: /etc/systemd/system/minio.service.d/

- name: Installing the systemd units
  become: true
  ansible.builtin.template:
    src: '{{ item }}.j2'
    dest: '/etc/systemd/system/{{ item }}'
    mode: '0644'
    owner: root
  with_items:
    - minio.service
    - minio.socket
    - minio_configure.service
  notify: "Restart minio"

- name: Enable minio service
  become: true
  ansible.builtin.systemd:
    name: 'minio'
    enabled: true
  with_items:
    - minio.socket
    - minio.service
    - minio_configure.service
