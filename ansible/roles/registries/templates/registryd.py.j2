#!/usr/bin/python3 -u

from dataclasses import dataclass, asdict
import os
import re
import pathlib
import subprocess
import time
import traceback

from inotify_simple import INotify, flags
import jinja2


def run_cmd(*cmd: list[str]) -> None:
    try:
        print("# " + ' '.join([f"'{a}'" for a in cmd]))
        subprocess.check_call(cmd)
    except subprocess.CalledProcessError as e:
        print(f"--> The command failed with the return code {e.returncode}")


@dataclass
class Registry:
    name: str
    port: int

    cfg_template: str = None
    proxy: str = None

    def __post_init__(self):
        if self.cfg_template is None:
            self.cfg_template = "{{ registryd_cfg }}/config.yml.j2"

        self.port = int(self.port)
        if self.port < 1024 and self.port > 65535:
            raise ValueError("Invalid port number. Accepted range: [1024, 65535]")

        # Fixup the name: Replace all non-alphanumeric characters with a `_`, and ensure
        # we don't have 2+ underscores in a row
        self.name = re.sub(r'[^\w]', '_', self.name)
        self.name = re.sub(r'_+', '_', self.name)

        self.state_on_disk = dict()
        self.needs_restart = True

    def __eq__(self, other):
        return asdict(self) == asdict(other)

    @property
    def unit_name(self):
        return f"{self.name}-registry"

    @property
    def proxy_unit_name(self):
        return f"{self.name}-registry-proxy"

    @property
    def base_mount(self):
        return f"{{ cache_mount }}/registries"

    @property
    def socket_path(self):
        return f"{self.base_mount}/{self.name}.socket"

    @property
    def mount_point(self):
        return f"{self.base_mount}/{self.name}"

    @property
    def executor_env_var(self):
        name = self.name.replace('-', '_').upper()
        return f"EXECUTOR_JOB_{name}_REGISTRY"

    @property
    def config_path(self):
        return f"{self.base_mount}/{self.name}_config.yml"

    @property
    def systemd_units(self):
        units = []
        for tmpl_filename in ["registry-proxy.service.j2",
                            "registry-proxy.socket.j2",
                            "registry.service.j2"]:
                dst_filename = self.name + "-" + tmpl_filename.removesuffix('.j2')
                units.append((tmpl_filename, f"/etc/systemd/system/{dst_filename}"))
        return units

    def gen_config(self):
        if asdict(self) != self.state_on_disk:
            print(f"{self.name}: Generating the systemd units")

            # Generate the boot configuration
            with open(self.cfg_template) as cfg_template:
                jinja2.Template(cfg_template.read()).stream(registry=self).dump(self.config_path)

            # Generate the systemd units
            env = jinja2.Environment(loader=jinja2.FileSystemLoader("{{ registryd_cfg }}"))
            for tmpl_filename, destination in self.systemd_units:
                tmpl = env.get_template(tmpl_filename)
                tmpl.stream(registry=self).dump(destination)

            self.state_on_disk = asdict(self)
            self.needs_restart = True

        return self.needs_restart

    def rm_config(self):
        for _, destination in self.systemd_units:
            pathlib.Path(destination).unlink(missing_ok=True)

    # Return true if the service was restarted
    def restart(self):
        if self.needs_restart:
            print(f"{self.name}: Restarting the {self.unit_name} and {self.proxy_unit_name} units")
            run_cmd("systemctl", "restart", f"{self.proxy_unit_name}.socket", f"{self.proxy_unit_name}", f"{self.unit_name}")
            self.needs_restart = False

    def stop(self):
        print(f"{self.name}: Stopping the {self.unit_name} and {self.proxy_unit_name} units")
        run_cmd("systemctl", "stop", f"{self.proxy_unit_name}.socket", f"{self.proxy_unit_name}.service", f"{self.unit_name}")

    @classmethod
    def from_user_file(cls, path):
        filename = os.path.basename(path)
        if filename.endswith(".yml.j2"):
            if m := re.match(r'(?P<port>\d+)_(?P<name>[\w-]+).yml.j2', filename):
                return cls(**m.groupdict(), cfg_template=path)
            else:
                print(f"WARNING: Found the ill-formed filename '{filename}'. Expected format: $port_$name.yml.j2")


def get_user_registries():
    registries = []

    print("Scanning for user-specified registries at {{ registryd_user_cfg }}")
    with os.scandir("{{ registryd_user_cfg }}") as it:
        for entry in it:
            try:
                if not entry.is_file():
                    continue

                if registry := Registry.from_user_file(entry.path):
                    registries.append(registry)
            except Exception:
                traceback.print_exc()

    return registries


def start_registries(registries):
    has_changes = False

    # Generate the config for all the registries
    with open(f"{{ registries_env_file }}", "w") as f:
        f.write("""# WARNING: Do not edit this file by hand as it gets re-generated
#
# WARNING: These environment variables are part of the ABI of the infrastructure
#          as they are exposed to executor jobs! To keep backwards compatibility,
#          NEVER CHANGE A VARIABLE, JUST ADD NEW ONE!
""")

        for registry in registries:
            # Generate the systemd config
            has_changes |= registry.gen_config()

            f.write(f"EXECUTOR_JOB__{registry.executor_env_var}={{hostname }}:{ registry.port}\n")

    # If any of the configuration files changed
    if has_changes:
        print("Reloading the systemd daemon")
        run_cmd("systemctl", "daemon-reload")

        # (Re)start all the registries
        for registry in registries:
            registry.restart()

        # The registries environment file may have changed, so restart the executor to update the job variables
        print("Restarting the executor")
        run_cmd("systemctl", "restart", "executor")


# Set up a watch
inotify = INotify()
watch_flags = flags.MOVED_FROM | flags.MOVED_TO | flags.CREATE | flags.MODIFY | flags.ATTRIB | flags.DELETE | flags.CLOSE_WRITE
inotify.add_watch("{{ registryd_user_cfg }}", watch_flags)

# Get the list of static registries that are bundled in the image
registries = [Registry(**r) for r in {{ registries.services }}]

# Add user-specified registries
registries += get_user_registries()

while True:
    # TODO: Detect when multiple registries have the same name or port

    start_registries(registries)

    print("Waiting for changes in {{ registryd_user_cfg }}")
    events = inotify.read(read_delay=1000)
    print("")

    # Go through the inotify events that happened on the folder
    for event in events:
        # Ignore any file change that isn't for a registry (invalid filename, or format)
        filepath = os.path.join("{{ registryd_user_cfg }}", event.name)
        registry = Registry.from_user_file(filepath)
        if registry is None:
            continue

        # Always remove any registry associated to a file that changed as either it got deleted or it's been modified
        # and thus requires a restart.
        try:
            registries.remove(registry)
        except ValueError:
            pass

        # Shutdown the registry associated to a file when moved or deleted
        if event.mask & (flags.MOVED_FROM | flags.DELETE):
            print(f"--> The registry file {event.name} was renamed or deleted")
            registry.stop()
            registry.rm_config()
        else:
            print(f"--> The registry file {event.name} was created/modified")
            registries.append(registry)
