#!/bin/bash
set -ex

. containers/build_functions.sh

build() {
    # (2023-02) Will be nice to use ci-templates, when we have landed support for
    # creating multi-arch builds and a default command

    case $platform in
        linux/arm64*)
            dnf install -y qemu-user-static-aarch64
            ;;
        linux/arm*)
            dnf install -y qemu-user-static-arm
            ;;
        linux/riscv64)
            dnf install -y qemu-user-static-riscv
            ;;
    esac

    buildcntr=$(buildah from --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS alpine:edge)
    buildmnt=$(buildah mount $buildcntr)

    $buildah_run $buildcntr apk add --no-cache python3 py3-pip

    mkdir -v $buildmnt/app
    cp -ar machine_registration gfxinfo $buildmnt/app/

    buildah config --workingdir /app $buildcntr

    $buildah_run $buildcntr pip3 install --no-cache-dir -r machine_registration/requirements.txt --break-system-packages
    $buildah_run $buildcntr pip3 install -e ./gfxinfo --break-system-packages

    # For production, cache the known PCI devices for into the container
    # to avoid external network requirements.
    $buildah_run $buildcntr python3 machine_registration/machine_registration.py cache

    # Remove all unecessary files
    $buildah_run $buildcntr find /usr/lib -name *.pyc -exec rm {} \;
    $buildah_run $buildcntr rm -rf /root/.cache
    $buildah_run $buildcntr apk del --no-cache py3-pip

    buildah config --entrypoint '["/app/machine_registration/machine_registration.py"]' --cmd 'register' $buildcntr
}

build_and_push_container
