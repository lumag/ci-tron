#!/bin/sh

set -eux

. containers/build_functions.sh

build() {
    # (2023-02) Will be nice to use ci-templates, when we have landed support for
    # creating multi-arch builds and a default command
    case $platform in
        linux/amd64)
            mcli_platform=amd64
            ;;
        linux/arm64*)
            mcli_platform=arm64
            ;;
        linux/arm*)
            mcli_platform=arm
            ;;
        *)
           echo "Unsupported platform $platform"
           exit 1
           ;;

    esac

    buildcntr=$(buildah from --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS alpine:latest)
    buildmnt=$(buildah mount $buildcntr)

    mkdir -p $buildmnt/app/executor/client
    cp -ar ./executor/client $buildmnt/app/executor

    $buildah_run $buildcntr apk add --no-cache bash coreutils python3 py3-pip py3-jinja2 wget curl

    $buildah_run $buildcntr python3 -m pip install --break-system-packages --upgrade pip
    $buildah_run $buildcntr pip install --break-system-packages /app/executor/client
    $buildah_run $buildcntr wget --progres=dot:mega -O /usr/bin/mcli https://dl.min.io/client/mc/release/linux-${mcli_platform}/mc
    $buildah_run $buildcntr chmod +x /usr/bin/mcli

    # Remove all unecessary files
    $buildah_run $buildcntr rm -rf /app/executor
    $buildah_run $buildcntr apk del --no-cache py3-pip
}

build_and_push_container
